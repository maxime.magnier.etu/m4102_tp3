package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Produces("application/json")
@Path("/pizzas")
public class PizzaRessource {
	
	private static final Logger LOGGER = Logger.getLogger(PizzaRessource.class.getName());

    private PizzaDao pizza;
    private IngredientDao ingredients;
    
    @Context
    public UriInfo uriInfo;
    
    public PizzaRessource() {
    	ingredients = BDDFactory.buildDao(IngredientDao.class);
    	ingredients.createTable();
        pizza = BDDFactory.buildDao(PizzaDao.class);
        pizza.createTablePizzaAndIngredientAssociation();
    }
    @GET
    public List<PizzaDto> getAll() {
        LOGGER.info("PizzaRessource:getAll");

        List<PizzaDto> l = pizza.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
        LOGGER.info(l.toString());
        return l;
    }
    @GET
    @Path("{id}")
    @Produces({ "application/json", "application/xml" })
    public PizzaDto getOnePizza(@PathParam("id") UUID id) {
        LOGGER.info("getOnePizza(" + id + ")");
        try {
            Pizza piza = pizza.findById(id);
            LOGGER.info(piza.toString());
            return Pizza.toDto(piza);
        } catch (Exception e) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }
    @POST
    public Response createPizza(PizzaCreateDto pizzasCreateDto) {
        Pizza existing = pizza.findByName(pizzasCreateDto.getName());
        if (existing != null) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }

        try {
            Pizza pizzas = Pizza.fromPizzasCreateDto(pizzasCreateDto);
            pizza.insertIntoPizzas(pizzas);
            PizzaDto pizzasDto = Pizza.toDto(pizzas);

            URI uri = uriInfo.getAbsolutePathBuilder().path(pizzas.getId().toString()).build();

            return Response.created(uri).entity(pizzasDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }

    }
    @DELETE
    @Path("{id}")
    public Response deletePizzas(@PathParam("id") UUID id) {
      if ( pizza.findById(id) == null ) {
        throw new WebApplicationException(Response.Status.NOT_FOUND);
      }
      try {
    	  pizza.deletePizzasByID(id);
      }catch(Exception E) {
    	  E.printStackTrace();
      }

      return Response.status(Response.Status.ACCEPTED).build();
    }
    @GET
    @Path("{id}/name")
    public String getPizzaName(@PathParam("id") UUID id) {
        Pizza pizzas = pizza.findById(id);

        if (pizzas == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        return pizzas.getName();
    }
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response createPizzas(@FormParam("name") String name) {
      Pizza existing = pizza.findByName(name);
      if (existing != null) {
        throw new WebApplicationException(Response.Status.CONFLICT);
      }

      try {
        Pizza pizzas = new Pizza();
        pizzas.setName(name);

        pizza.insertPizzas(pizzas);

        PizzaDto pizzasDto = Pizza.toDto(pizzas);

        URI uri = uriInfo.getAbsolutePathBuilder().path("" + pizzas.getId()).build();

        return Response.created(uri).entity(pizzasDto).build();
      } catch (Exception e) {
          e.printStackTrace();
          throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
      }
    }

}
