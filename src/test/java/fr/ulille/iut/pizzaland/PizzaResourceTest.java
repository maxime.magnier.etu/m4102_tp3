package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class PizzaResourceTest extends JerseyTest{
	private PizzaDao pizzaDao;
	private IngredientDao ingredientDao;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();

		return new ApiV1();
	}

	@Before
	public void setEnvUp() {
		pizzaDao = BDDFactory.buildDao(PizzaDao.class);
		ingredientDao = BDDFactory.buildDao(IngredientDao.class);
		ingredientDao.createTable();
		pizzaDao.createTablePizzaAndIngredientAssociation();
	}

	@After
	public void tearEnvDown() throws Exception {
		pizzaDao.dropTableAndIngredientAssociation();
		ingredientDao.dropTable();
	}
	
	@Test
    public void testGetEmptyList() {
        // La méthode target() permet de préparer une requête sur une URI.
        // La classe Response permet de traiter la réponse HTTP reçue.
        Response response = target("/pizzas").request().get();

        // On vérifie le code de la réponse (200 = OK)
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        // On vérifie la valeur retournée (liste vide)
        // L'entité (readEntity() correspond au corps de la réponse HTTP.
        // La classe jakarta.ws.rs.core.GenericType<T> permet de définir le type
        // de la réponse lue quand on a un type paramétré (typiquement une liste).
        List<PizzaDto> pizzas;
        pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
        });

        assertEquals(0, pizzas.size());
    }
	
	@Test
	public void testGetExistingPizzas() {
		Pizza pizza = new Pizza();
		pizza.setName("Tartiflette");
		pizzaDao.insertPizzas(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		//Pizzas result = Pizzas.fromDto(response.readEntity(PizzasDto.class));
		Pizza result = pizzaDao.findById(pizza.getId());
		assertEquals(pizza, result);
	}
	
	@Test
	public void testGetNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}
	
	@Test
	public void testCreatePizzas() {
		PizzaCreateDto pizzasCreateDto = new PizzaCreateDto();
		pizzasCreateDto.setName("Saucisson");

		Response response = target("/pizzas").request().post(Entity.json(pizzasCreateDto));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

		assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
		assertEquals(returnedEntity.getName(), pizzasCreateDto.getName());
	}

	@Test
	public void testCreateSamePizzas() {
		Pizza pizza = new Pizza();
		pizza.setName("Tartiflette");
		ingredientDao.insert(new Ingredient("Reblochon"));
		List<Ingredient> ing = new ArrayList<>();
		ing = ingredientDao.getAll();
		pizza.setIngredients(ing);
		pizzaDao.insertPizzas(pizza);

		PizzaCreateDto pizzasCreateDto = Pizza.toCreateDto(pizza);
		Response response = target("/pizzas").request().post(Entity.json(pizzasCreateDto));

		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreatePizzaWithoutName() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testDeleteExistingPizzas() {
		Pizza pizza = new Pizza();
		pizza.setName("Tartiflette");
		ingredientDao.insert(new Ingredient("Reblochon"));
		ingredientDao.insert(new Ingredient("Pomme de terre"));
		List<Ingredient> ing = new ArrayList<>();
		ing = ingredientDao.getAll();
		pizza.setIngredients(ing);
		pizzaDao.insertIntoPizzas(pizza);

		Response response = target("/pizzas/").path(pizza.getId().toString()).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Pizza result = pizzaDao.findById(pizza.getId());
		assertEquals(result, null);
	}
	
	@Test
	public void testDeleteNotExistingIngredient() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testGetNotExistingPizzasName() {
		Response response = target("pizzas").path(UUID.randomUUID().toString()).path("name").request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	

	@Test
	public void testGetIngredientNameFromPizzas() {
		
		Pizza pizza = new Pizza();
		pizza.setName("Tartiflette");
		
		ingredientDao.insert( new Ingredient("Reblochon"));
		ingredientDao.insert( new Ingredient("Pomme de Terre"));
		
		List<Ingredient> ingList = new ArrayList<>();
		ingList = ingredientDao.getAll();
		pizza.setIngredients(ingList);
		
		
		
		pizzaDao.insertIntoPizzas(pizza);
		
		Response response = target("pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		
		assertEquals(pizza, result);
	}
	
	
	@Test
	public void testGetNotExistingIngredientName() {
		Response response = target("pizzas").path(UUID.randomUUID().toString()).path("name").request().get();

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	/*

	
	@Test
	public void testCreateWithForm() {
		Form form = new Form();
		form.param("name", "chorizo");

		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		Response response = target("ingredients").request().post(formEntity);

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		String location = response.getHeaderString("Location");
		String id = location.substring(location.lastIndexOf('/') + 1);
		Ingredient result = dao.findById(UUID.fromString(id));
		assertNotNull(result);
	}*/
}
